var grid_size = 8;

var dragOpts ={
    helper: 'clone',
    zIndex: 10,
    revert: "invalid",
    stack: ".layout",
    snap: ".layout, .item",
    snapMode: "both",
    start: function(event,ui){ // item in item-list : drag start event (when you click item)
        
    },
    drag: function(event,ui){ // item in item-list : dragging event (when you drag item)
        var $el = $('.ui-draggable-dragging')
        var $elSibs = $('.layout').find('.item')
        $elSibs.each(function(){
            var $sib = $(this)
            collision($sib, $el)
        })
    },
}

var xSave , ySave
var dragOpts_on_layout ={
    grid : [grid_size , grid_size],
    snap: ".layout, .item",
    snapMode: "both",
    start: function(event,ui){ // item in layout : drat start event (when you click item)
        xSave = $(this).position().left
        ySave = $(this).position().top
    },
    drag: function(event,ui){  // item in layout : dragging event (when you drag item)

        var $el = $(this)
        var $elSibs = $(this).siblings('.item')
        $elSibs.each(function(){
            var $sib = $(this)
            collision($sib, $el)
        })
    },
    stop: function(event,ui){ // item in layout : drag end event (after you drop item)
        var $el = $(this)
        var $elSibs = $(this).siblings('.item')

        $elSibs.each(function(){
            var $sib = $(this)
            collision($sib, $el)
            var result = collision($sib, $el)
            if(result == true){
                $el.css({'top':ySave, 'left':xSave})
                $sib.find('img').removeClass('collision')
            }
        })

        $el.addClass('bounce-in')
        setTimeout(function(){$el.removeClass('bounce-in')} , 500)
    }
}

var dropOpts ={
    tolerance: 'fit',
    activeClass: "layout-active",
    hoverClass:  "layout-hover",
    drop: function(e, ui){
        if(ui.draggable.hasClass('item')){
            var m_parent = ui.draggable.parent()
            if(m_parent.prop("tagName") == 'LI'){  // item in item-list : drop event (when you drop item to layout)

                var cloneItem = ui.draggable.clone()
                
                cloneItem.css({
                    position: 'absolute',
                    top: parseInt((ui.offset.top - $(this).offset().top) / grid_size)* grid_size,
                    left: parseInt((ui.offset.left - $(this).offset().left) / grid_size)* grid_size
                }).draggable(dragOpts_on_layout)
                
                cloneItem.append('<div class="close-bt"><span>x</span></div>')
                
                $(this).append(cloneItem)
                
                var $elSibs = $(cloneItem).siblings('.item')

                $elSibs.each(function(){
                    var $sib = $(this)
                    var result = collision($sib, $(cloneItem))
                    if(result == true){
                        $(cloneItem).remove()
                        $elSibs.each(function(){
                            $(this).find('img').removeClass('collision')        
                        })
                        return false
                    }
                })

                $(cloneItem).addClass('bounce-in')
                setTimeout(function(){$(cloneItem).removeClass('bounce-in')} , 500)
            }
        }
    },
    
}

$(document).ready(function(){
	
	$('.item-list .item').each(function(index){
		$(this).draggable(dragOpts)
	})

	$('.layout').droppable(dropOpts)

})

$(document).on('click' , '.close-bt' , function(){
    $(this).parent().remove();
})

$(document).on('dblclick' , '.layout .item' , function(){ // double click event : rotate item 
    var current_deg = $(this).attr('rotation')
    $(this).attr('rotation' , parseInt(current_deg) + 90)
    item = $(this)
// transform: translateX(-50%) translateY(-50%) rotate(90deg);
    $(this).find('img').animate({
            deg: parseInt(current_deg) + 90
        },
        {
            duration: 10,
            step: function(now){
                $(this).css({ transformOrigin: 'center'})
                $(this).css({ transform: 'rotate(' + now + 'deg)'})
            }
        }
    )
    
    x = $(this).position().left
    y = $(this).position().top
    w = $(this).width()
    h = $(this).height()

    $(this).css({width:h , height:w})

    if(current_deg == 0){
        $(this).find('img').css({position:'absolute',left: -($(this).height() - $(this).width()) / 2, top: ($(this).height() - $(this).width()) / 2})
    }else if(current_deg == 90){
        $(this).find('img').css({position:'absolute',left: 0, top: 0})
    }else if(current_deg == 180){
        $(this).find('img').css({position:'absolute',left: -($(this).height() - $(this).width()) / 2, top: ($(this).height() - $(this).width()) / 2})
    }else if(current_deg == 270){
        $(this).find('img').css({position:'absolute',left: 0, top: 0})
    }
    
    if(parseInt(current_deg) == 0 || parseInt(current_deg) == 180){
        $(this).find('img').css({width: item.height() , height: item.width()})
    }else {
        $(this).find('img').css({width: item.width() , height: item.height()})
    }

    $(this).css({position:'absolute',left: item.position().left + (item.height() - item.width()) / 2, top: item.position().top -(item.height() - item.width()) / 2})
    
    if(current_deg == 270){
        $(this).attr('rotation' ,  0)
    }
    
})

// Collision detection
function collision($item, $el){
       
    var sibInner = $item
    var wigInner = $el

    var x1 = wigInner.offset().left - 1
    var y1 = wigInner.offset().top - 1
    var h1 = wigInner.outerHeight(true) - 2
    var w1 = wigInner.outerWidth(true) - 2

    var x2 = sibInner.offset().left - 1
    var y2 = sibInner.offset().top - 1 
    var h2 = sibInner.outerHeight(true) - 2
    var w2 = sibInner.outerWidth(true) - 2

    var obj1 = new Polygon( {"x":x1 + w1 / 2, "y": y1 + h1 / 2 }, "#00FF00" );
    
    obj1.addPoint({"x":-w1 / 2, "y":-h1 / 2});
    obj1.addPoint({"x":w1 / 2, "y":-h1 / 2});
    obj1.addPoint({"x":w1 / 2, "y":h1 / 2});
    obj1.addPoint({"x":-w1 / 2, "y":h1 / 2});
    // obj1.rotate(parseInt(wigInner.attr('rotation')) * (Math.PI / 180))

    var obj2 = new Polygon( {"x":x2 + w2 / 2, "y": y2 + h2 / 2 }, "#00FF00" );
    obj2.addPoint({"x":-w2 / 2, "y":-h2 / 2});
    obj2.addPoint({"x":w2 / 2, "y":-h2 / 2});
    obj2.addPoint({"x":w2 / 2, "y":h2 / 2});
    obj2.addPoint({"x":-w2 / 2, "y":h2 / 2});
    // obj2.rotate(parseInt(sibInner.attr('rotation')) * (Math.PI / 180))

    if(obj1.intersectsWith(obj2))
    {
        sibInner.find('img').addClass('collision')
        return true
    } else 
    { 
        sibInner.find('img').removeClass('collision')
    }

    return false
}